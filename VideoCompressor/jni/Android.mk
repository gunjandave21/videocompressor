LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
LOCAL_MODULE    := ffmpegrun
LOCAL_SRC_FILES := ffmpegrun.c
LOCAL_C_INCLUDES += $(LOCAL_PATH)/
LOCAL_LDLIBS := -llog -ljnigraphics -lz 
LOCAL_SHARED_LIBRARIES := libavutil libavcodec libavformat libswresample libswscale  libpostproc libavfilter  
include $(BUILD_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE    := VideoCompressor
LOCAL_SRC_FILES := VideoCompressor.c
LOCAL_LDLIBS := -llog -ljnigraphics -lz 
include $(BUILD_SHARED_LIBRARY)

$(call import-module,ffmpeg-2.2.4/android/arm)
